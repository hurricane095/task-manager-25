package ru.krivotulov.tm.exception.field;

/**
 * LoginEmptyException
 *
 * @author Aleksey_Krivotulov
 */
public final class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Error! login is empty...");
    }

}
