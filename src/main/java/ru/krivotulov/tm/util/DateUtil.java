package ru.krivotulov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    String PATTERN = "dd.MM.yyyy";

    @NotNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @Nullable
    static Date toDate(@Nullable final String date) {
        if (date == null || date.isEmpty()) return null;
        try {
            return FORMATTER.parse(date);
        } catch (@NotNull final ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @NotNull
    static String toString(@Nullable final Date date) {
        if (date == null) return "";
        return FORMATTER.format(date);
    }

}
