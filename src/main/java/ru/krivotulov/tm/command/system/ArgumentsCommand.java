package ru.krivotulov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.model.ICommand;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.util.ListUtil;

import java.util.Collection;
import java.util.Optional;

/**
 * ArgumentsCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ArgumentsCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Display argument list.";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        Optional.ofNullable(ListUtil.emptyListToNull(commands))
                .ifPresent(list -> {
                    list.stream()
                            .filter(command -> command.getArgument() != null && !command.getArgument().isEmpty())
                            .forEach(System.out::println);
                });
    }

}
