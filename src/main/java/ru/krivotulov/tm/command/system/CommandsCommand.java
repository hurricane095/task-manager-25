package ru.krivotulov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.util.ListUtil;

import java.util.Collection;
import java.util.Optional;

/**
 * CommandsCommand
 *
 * @author Aleksey_Krivotulov
 */
public class CommandsCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String DESCRIPTION = "Display command list.";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        Optional.ofNullable(ListUtil.emptyListToNull(commands))
                .ifPresent(list -> {
                    list.stream()
                            .filter(command -> !command.getName().isEmpty())
                            .forEach(System.out::println);
                });
    }

}
