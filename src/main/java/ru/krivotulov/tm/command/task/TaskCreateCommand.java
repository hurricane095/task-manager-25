package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Date;

/**
 * TaskCreateCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    public static final String DESCRIPTION = "Create new task.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.readLine();
        System.out.println("ENTER DATE BEGIN: ");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END: ");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final String userId = getUserId();
        getTaskService().create(userId, name, description, dateBegin, dateEnd);
    }

}
