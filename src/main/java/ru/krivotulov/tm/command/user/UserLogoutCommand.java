package ru.krivotulov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.enumerated.Role;

/**
 * UserLogoutCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-logout";

    @NotNull
    public static final String DESCRIPTION = "Log out.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().logout();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
