package ru.krivotulov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserUpdateCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserUpdateCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-update";

    @NotNull
    public static final String DESCRIPTION = "User update.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull String userId = getAuthService().getUserId();
        System.out.println("ENTER FIRST NAME: ");
        @Nullable String firstName = TerminalUtil.readLine();
        System.out.println("ENTER LAST NAME: ");
        @Nullable String lastName = TerminalUtil.readLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @Nullable String middleName = TerminalUtil.readLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
